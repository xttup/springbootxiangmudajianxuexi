package com.xtt.demo.service;

import com.xtt.SpringbootApplicationTests;
import com.xtt.demo.model.User;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.BoundSetOperations;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import redis.clients.jedis.JedisPoolConfig;

import static org.junit.Assert.*;

/**
 * @Author xtt.
 * @Description:
 * @Date:Create in 2018/1/3 0003 下午 22:37
 * @Modify By:
 */
public class RedisServiceTest extends SpringbootApplicationTests {

    @Autowired
    RedisService redisService;

    @Autowired
    JedisConnectionFactory jedisConnectionFactory;

    @Autowired
    public RedisTemplate redisTemplate;

    @Autowired
    public StringRedisTemplate stringRedisTemplate;


    @Test
    public void testss(){
        System.out.println(jedisConnectionFactory.getPoolConfig().getMaxIdle());
        System.out.println(jedisConnectionFactory.getUsePool());
    }
    @Test
    public void testSerializer() {
        System.out.println("redisTemplate.getKeySerializer():"+redisTemplate.getKeySerializer());
        System.out.println("redisTemplate.getValueSerializer():"+redisTemplate.getValueSerializer());
        System.out.println("stringRedisTemplate.getValueSerializer():"+stringRedisTemplate.getKeySerializer());
        System.out.println("stringRedisTemplate.getValueSerializer():"+stringRedisTemplate.getValueSerializer());
        User user = new User();
        user.setName("ddd");
        user.setId(1);
        BoundValueOperations valueOps = redisTemplate.boundValueOps("test-key");
        valueOps.set(user);
        System.out.println("user:"+valueOps.get());

    }

    @Test
    public void testString() {
        redisService.valueAdd("string-key",123);
        System.out.println(redisService.valueGet("string-key"));
    }

    @Test
    public void testList() {
        redisService.listLeftPush("list-key","list");
        redisService.listLeftPush("list-key","list2");
        System.out.println(redisService.listLeftPop("list-key"));
        System.out.println(redisService.listLeftPop("list-key"));
    }

    @Test
    public void testTransac() {
        BoundSetOperations setOps = redisTemplate.boundSetOps("tranc-key");
        System.out.println("tranc-key size:"+setOps.size());//0
        try{

            System.out.println(redisService.actInTransaction());
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("tranc-key size:"+setOps.size());//无异常2 异常0,->事务
        redisTemplate.delete("tranc-key");
        System.out.println("tranc-key size:"+setOps.size());
    }

    @Test
    public void testTransac2() {
        BoundSetOperations setOps = redisTemplate.boundSetOps("tranc-key");
        System.out.println("tranc-key size:"+setOps.size());//0
        try{

            redisService.actInTransactionst2();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("tranc-key size:"+setOps.size());//无异常2 异常0,->事务
        redisTemplate.delete("tranc-key");
        System.out.println("tranc-key size:"+setOps.size());
    }

}