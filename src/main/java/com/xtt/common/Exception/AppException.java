package com.xtt.common.Exception;

/**
 * @Author xtt.
 * @Description: 应用级异常类
 * @Date:Create in 2017/5/29 0029 下午 13:44
 * @Modify By:
 */
public class AppException extends RuntimeException {
    public AppException() {
    }

    public AppException(String message) {
        super(message);
    }

    public AppException(String message, Throwable cause) {
        super(message, cause);
    }

    public AppException(Throwable cause) {
        super(cause);
    }

    public AppException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
