package com.xtt.common.entity;
import java.io.Serializable;

public class JsonResult implements Serializable {
    private static final long serialVersionUID = 4521539632282878363L;

    private boolean success;
    private String status;
    private String msg;
    private Object result;

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public JsonResult(boolean success, String status, String msg) {
        this.success = success;
        this.status = status;
        this.msg = msg;
    }

    public JsonResult() {
        this.setSuccess(true);
        this.setStatus("true");
        this.setMsg("true");
    }

    public void getFailResult(){
        this.setSuccess(false);
        this.setStatus("false");
        this.setMsg("false");
        this.setResult(null);
    }
}