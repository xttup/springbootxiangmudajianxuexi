package com.xtt.demo.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @Author xtt.
 * @Description: 定时任务
 * @Date:Create in 2017/5/29 0029 下午 13:32
 * @Modify By:
 */
@Component
public class TaskExample {

    //@Scheduled(cron="0/2 * * * * ?")
    public void scheduledTask1() {
        System.out.println("执行定时任务。。。");
    }
}
