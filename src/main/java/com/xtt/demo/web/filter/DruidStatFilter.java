//package com.xtt.demo.web.filter;
//
//import com.alibaba.druid.support.http.WebStatFilter;
//
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.annotation.WebInitParam;
//
///**
// * @Author xtt.
// * @Description: filter两种配置方式其一，另一种见DruidDBConfig.java
// * @Date:Create in 2017/5/29 0029 下午 14:57
// * @Modify By:
// */
//@WebFilter(filterName="druidWebStatFilter",urlPatterns="/*",
//        initParams={
//                @WebInitParam(name="exclusions",value="*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/druid/*")// 忽略资源
//        })
//public class DruidStatFilter extends WebStatFilter {
//
//}
