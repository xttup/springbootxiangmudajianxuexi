package com.xtt.demo.repository;

import com.xtt.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @Author xtt.
 * @Description:
 * @Date:Create in 2017/5/29 0029 下午 15:47
 * @Modify By:
 */
public interface UserResository extends JpaRepository<User,Integer>{
    List<User> findByName(String name);
    User findById(Integer id);
}
