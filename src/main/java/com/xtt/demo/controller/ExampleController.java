package com.xtt.demo.controller;

import com.xtt.common.Exception.AppException;
import com.xtt.demo.model.Example;
import com.xtt.demo.service.IRetryExample;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author xtt.
 * @Description: 使用测试
 * @Date:Create in 2017/5/29 0029 上午 10:01
 * @Modify By:
 */
@RestController
public class ExampleController extends BaseController{

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Value("${uploadfile}")
    private String uploadfile;
    @Autowired
    private Example example;
    @Autowired
    private IRetryExample retryExample;

    @GetMapping("/test")
    public String test(){
        return uploadfile;
    }

    @GetMapping("/test2")
    public Example test2(){
        logger.error("test:"+example);
        return example;
    }

    @GetMapping("/test3/{id}")
    public Integer test3(@PathVariable("id") Integer id){
        System.out.println(id);
        return id;
    }

    /**
     * 重试测试
     */
    @GetMapping("/testRetry")
    public void testRetry() {
        try {
            retryExample.retryTest();
        } catch (AppException e) {
            e.printStackTrace();
        }
    }
}
