package com.xtt.demo.model;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @Author xtt
 * @Description:
 * @Date:Create in 2017/5/29 0029 上午 10:43
 * @Modify By:
 */
@Component
@ConfigurationProperties(prefix = "example")
public class Example {
    private Integer id;
    private String name;

    private Example2 example2 = new Example2();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Example2 getExample2() {
        return example2;
    }

    public static class Example2{
        private String tel;

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }
    }
}
