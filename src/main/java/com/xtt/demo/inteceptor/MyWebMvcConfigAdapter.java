package com.xtt.demo.inteceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * @Author xtt.
 * @Description:
 * @Date:Create in 2017/5/29 0029 下午 19:31
 * @Modify By:
 */
@Configuration
public class MyWebMvcConfigAdapter extends WebMvcConfigurerAdapter {
    /**
     * 配置拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor())
                .addPathPatterns("/**")
                .excludePathPatterns("/toLogin","/login");
        super.addInterceptors(registry);
    }

    /**
     * 配置静态资源访问
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //增加静态资源访问
        super.addResourceHandlers(registry);
    }
}
