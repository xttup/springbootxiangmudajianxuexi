//package com.xtt.demo.config;
//
//import com.xtt.demo.model.Example;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.EnableConfigurationProperties;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;
//
///**
// * @Author xtt.
// * @Description: properties 解析并放入容器的一种方式，或直接用@Component
// * @Date:Create in 2018/1/5 0005 下午 22:51
// * @Modify By:
// */
//@Configuration
//@EnableConfigurationProperties(Example.class)
//public class ExampleConfig {
//
//    @Autowired
//    private Example example;
//}
