package com.xtt.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @Author xtt.
 * @Description: 在自动配置完成后替换RedisTemplate 以增加一些自定义配置 如开启事务注解
 * @Date:Create in 2018/1/4 0004 下午 23:31
 * @Modify By:
 */
@Configuration
@Profile("test2")//表示测试的时候开启对应spring.profiles.active
@AutoConfigureAfter(RedisAutoConfiguration.class)
public class RedisConfig2 {

    @Autowired
    RedisTemplate redisTemplate;

    @Bean
    public RedisTemplate<Object, Object> redisTemplate() {
        redisTemplate.setEnableTransactionSupport(true);
        return redisTemplate;
    }
}
