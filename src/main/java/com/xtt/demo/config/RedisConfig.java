package com.xtt.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import redis.clients.jedis.JedisPoolConfig;

/**
 * @Author xtt.
 * @Description:替换自动配置
 * @Date:Create in 2018/1/4 0004 下午 22:42
 * @Modify By:
 */
@Configuration//关闭
@Profile("test1")//表示测试的时候开启对应spring.profiles.active
public class RedisConfig {

    @Value("${spring.redis.pool.max-idle}")
    private Integer maxIdle;
    //.....
    @Bean
    public RedisTemplate<Object, Object> redisTemplate() {
        RedisTemplate<Object,Object> redisTemplate = new RedisTemplate();
        redisTemplate.setConnectionFactory(jedisConnectionFactory());
        redisTemplate.setEnableTransactionSupport(true);//开启redis事务注解
        return redisTemplate;
    }

    @Bean
    public JedisConnectionFactory jedisConnectionFactory(){
        JedisConnectionFactory jedisConnectionFactory = new JedisConnectionFactory();
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisConnectionFactory.setPoolConfig(jedisPoolConfig);
        return jedisConnectionFactory;
    }
}
