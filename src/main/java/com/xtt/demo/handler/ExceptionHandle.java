package com.xtt.demo.handler;

import com.xtt.common.entity.JsonResult;
import com.xtt.demo.controller.BaseController;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Author xtt.
 * @Description: 全局异常处理
 * @Date:Create in 2017/5/29 0029 下午 18:17
 * @Modify By:
 */
@ControllerAdvice
@ResponseBody
public class ExceptionHandle extends BaseController{

    @ExceptionHandler(value = Exception.class)
    public JsonResult handleException(Exception e) {
        return renderError(e.getCause().getMessage());
    }

}
