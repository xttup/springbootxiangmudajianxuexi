package com.xtt.demo.service;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 * @Author chentao.
 * @Description:
 * @Date:Create in 2017/5/13
 * @Modify By:
 */
public interface IAopExample {
    void doAccessCheck(String name);
    void doAfter();
    void after();
    void doAfterThrow();
    Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable;
}
