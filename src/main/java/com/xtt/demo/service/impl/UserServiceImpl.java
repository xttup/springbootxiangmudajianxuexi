package com.xtt.demo.service.impl;

import com.xtt.common.Exception.AppException;
import com.xtt.demo.model.User;
import com.xtt.demo.repository.UserResository;
import com.xtt.demo.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author xtt.
 * @Description:
 * @Date:Create in 2017/5/29 0029 下午 15:49
 * @Modify By:
 */
@Service
@Transactional
public class UserServiceImpl extends BaseServiceImpl implements IUserService {

    @Autowired
    private UserResository userResository;

    /**
     * 查询所有
     * @return
     */
    @Override
    public List<User> findAll() {
        return userResository.findAll();
    }

    @Override
    public List<User> findByName(String name) {
        return userResository.findByName(name);
    }

    /**
     * 事务测试
     */
    @Override
    public void insert() throws AppException {
        userResository.save(new User("11","18800000000"));
        if(true) {
            throw new AppException("tes");
        }
        userResository.save(new User("22","22"));


    }


    @Override
    public void insert(User user) {
        userResository.saveAndFlush(user);
    }

    /**
     * aop测试
     */
    @Override
    public void testAop() {
        System.out.println("执行方法。。");
    }

    /**
     * 全局异常测试
     * @return
     */
    @Override
    public User testExceptionHandle() throws AppException {
        if(true) {
            throw new AppException("test Exception");
        }
        return userResository.findById(1);
    }


}
