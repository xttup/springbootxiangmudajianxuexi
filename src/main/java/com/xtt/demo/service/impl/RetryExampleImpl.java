package com.xtt.demo.service.impl;

import com.xtt.common.Exception.AppException;
import com.xtt.demo.service.IRetryExample;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;

/**
 * @Author xtt.
 * @Description: 异常重试使用实例
 * @Date:Create in 2017/5/29 0029 下午 13:41
 * @Modify By:
 */
@Component
public class RetryExampleImpl extends BaseServiceImpl implements IRetryExample{

    @Retryable(value = {AppException.class},maxAttempts = 4,backoff = @Backoff(delay = 1000, multiplier = 1))
    @Override
    public void retryTest() throws AppException {
        System.out.println("执行需要异常重试动作");
        throw new AppException("发生异常");
    }

    @Recover
    public void recover(AppException e) {
        System.out.println("重试失败");
    }

//        @Retryable注解
//        被注解的方法发生异常时会重试
//        value:指定发生的异常进行重试
//        include:和value一样，默认空，当exclude也为空时，所有异常都重试
//        exclude:指定异常不重试，默认空，当include也为空时，所有异常都重试
//        maxAttemps:重试次数，默认3
//        backoff:重试补偿机制，默认没有
//
//        @Backoff注解
//        delay:指定延迟后重试
//        multiplier:指定延迟的倍数，比如delay=5000l,multiplier=2时，第一次重试为5秒后，第二次为10秒，第三次为20秒
}
