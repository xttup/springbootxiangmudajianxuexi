package com.xtt.demo.service.impl;

import com.xtt.demo.service.IAopExample;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Service;

/**
 * @Author chentao.
 * @Description: 例
 * @Date:Create in 2017/5/13
 * @Modify By:
 */
@Service
@Aspect
public class AopExampleImpl implements IAopExample {
    @Pointcut("execution(* com.xtt.demo.service.IUserService.testAop(..))")
    private void aspectjMethod(){}//定义一个切入点

    @Before("aspectjMethod() && args(name)")//多个参数逗号相隔
    public void doAccessCheck(String name){
        System.out.println(name);
        System.out.println("前置通知");
    }

    @AfterReturning("aspectjMethod()")
    public void doAfter(){
        System.out.println("后置通知"); //抛出异常不执行
    }

    @After("aspectjMethod()")
    public void after(){
        System.out.println("最终通知");
    }

    @AfterThrowing("aspectjMethod()")
    public void doAfterThrow(){
        System.out.println("例外通知");//抛出异常执行
    }

    @Around("aspectjMethod()")
    public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable{
        System.out.println("进入环绕通知");
        Object object = pjp.proceed();//执行该方法
        System.out.println("退出方法");//抛出异常不执行
        return object;
    }

    /**
     * 无异常

     进入环绕通知
     {
     执行方法内容
     }
     退出方法
     最终通知
     后置通知

     有异常

     进入环绕通知
     {
     执行方法内容
     }
     最终通知
     例外通知
     */
}
