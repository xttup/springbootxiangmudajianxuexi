package com.xtt.demo.service;

import com.xtt.common.Exception.AppException;
import com.xtt.demo.model.User;
import com.xtt.demo.service.IBaseService;

import java.util.List;

/**
 * @Author xtt.
 * @Description:
 * @Date:Create in 2017/5/29 0029 下午 15:49
 * @Modify By:
 */
public interface IUserService extends IBaseService {
    List<User> findAll();

    List<User> findByName(String name);

    void insert() throws AppException;

    void insert(User user);

    void testAop();

    User testExceptionHandle() throws AppException;
}
