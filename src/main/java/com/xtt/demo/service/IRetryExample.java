package com.xtt.demo.service;

import com.xtt.common.Exception.AppException;

/**
 * @Author xtt.
 * @Description:
 * @Date:Create in 2017/5/29 0029 下午 13:40
 * @Modify By:
 */
public interface IRetryExample extends IBaseService{

    void retryTest() throws AppException;

    void recover(AppException e);
}
