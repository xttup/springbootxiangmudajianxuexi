package com.xtt.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.core.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author xtt.
 * @Description:
 * @Date:Create in 2018/1/3 0003 下午 21:32
 * @Modify By:
 */
@Service
public class RedisService {

    @Autowired
    public RedisTemplate redisTemplate;

    @Resource(name="redisTemplate")
    private ListOperations listOps;

    @Resource(name="redisTemplate")
    private HashOperations hashOps;

    @Resource(name="redisTemplate")
    private ValueOperations valueOps;

    @Resource(name="redisTemplate")
    private SetOperations setOps;

    @Resource(name="redisTemplate")
    private ZSetOperations zSetOps;


    //String
    public void valueAdd(String ops,Object value) {
//        redisTemplate.boundValueOps(ops).set(value);
        valueOps.set(ops,value);
    }
    public Object valueGet(String ops) {
//        return redisTemplate.boundValueOps(ops).get();
        return valueOps.get(ops);
    }

    //set
    public void setAdd(String ops,String value) {
        redisTemplate.boundSetOps(ops).add(value);
    }
    public void setPop(String ops) {
        redisTemplate.boundSetOps(ops).pop();
    }

    //list
    public void listLeftPush(String ops,Object value) {
//        redisTemplate.boundListOps(ops).leftPush(value);
        listOps.leftPush(ops,value);
    }

    public Object listLeftPop(String ops) {
//        return redisTemplate.boundListOps(ops).leftPop();
        return listOps.leftPop(ops);
    }

    //hash
    public void hashPut(String ops,Object key,Object value){
        redisTemplate.boundHashOps(ops).put(key,value);
    }

    public Object hashGet(String ops,Object key) {
        return redisTemplate.boundHashOps(ops).get(key);
    }

    //zset
    public void zsetAdd(String ops,double score,String member) {
        redisTemplate.boundZSetOps(ops).add(member,score);
    }

    public double zsetGetScore(String ops,String member) {
        return redisTemplate.boundZSetOps(ops).score(member);
    }

    //redis transaction
    public List<String> actInTransaction() {//返回每天redis语句的执行结果集合
        return (List<String>)redisTemplate.execute(new SessionCallback<List<String>>() {
            @Override
            public List<String> execute(RedisOperations redisOperations) throws DataAccessException {
                redisOperations.multi();
                BoundSetOperations setOps = redisOperations.boundSetOps("tranc-key");
                setOps.add("1");
                System.out.println("beforeExec size："+ setOps.size());//null
                setOps.add("2");
//                if(true){
//                    throw new RuntimeException("test");
//                }
                return (List<String>)redisOperations.exec();
            }
        });
    }

    @Transactional
    public void actInTransactionst2() {
        BoundSetOperations setOps1 = redisTemplate.boundSetOps("tranc-key");
        setOps1.add("1");
        System.out.println("beforeExec size："+ setOps1.size());
        setOps1.add("2");
//                if(true){
//                    throw new RuntimeException("test");
//                }
    }
}
